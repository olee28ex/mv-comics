import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CharacterPageComponent } from './pages/character-page/character-page.component';
import { PageNotFoundComponent } from './pages/page-notfound/page-notfound.component';


const routes: Routes = [
 { path: '', component: HomePageComponent },
 { path: 'character/:id', component: CharacterPageComponent },
 { path: '**', component: PageNotFoundComponent },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
