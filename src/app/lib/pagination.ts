import * as _ from 'underscore';

export class Pagination {
	offset:number =  0;
	limit: number = 10;
	total: number =  0;
	count: number =  0;
	numpage:number =  1;
	min: number = 1;

	get total_pages() { return Math.ceil(this.total / this.limit);	}

	getOffset() {
		return (this.numpage-1)*this.limit;
	}

	links() {
		let pg = Math.ceil(this.numpage/5);
		let min = ((pg-1)*5)+1;
		let max = min+4;
		let lnks:any = [];
		lnks.push({page: min-1, label: '', active: false, class: 'btnl' });
		for(let i = min; i <= max; i++) {
			lnks.push({page: i, label: i, class: ( i == this.numpage ? 'active' : '') });
		}
		lnks.push({page: this.numpage+1, label: '', class: 'btnr' });
		return lnks;
	}
}