
export default class Character {
	
	id: string;
	name: string;
	desc: string;
	modified: Date;
	thumbnail: {
		path: string,
		extension: string
	};
	resourceURI: string;
	comics: any[] = [];
	series: any[] = [];
	stories: any[] = [];
	events: any[] = [];
	urls: any[] = [];

	set description(val:string) { this.desc = val; }
	get description() {
		return (this.desc && this.desc.length > 0 ) ? this.desc : 'Character without description.';
	}

	getAlias() { return this.name.split('(')[0]; }
	getPicture() {
		let t = this.thumbnail;
		return `${t.path}.${t.extension}`;
	}

	static parse(vjson:any) {
		let newCharacter = new Character();
		newCharacter.id = vjson.id;
		newCharacter.name = vjson.name;
		newCharacter.desc = vjson.description;
		newCharacter.modified = vjson.modified;
		newCharacter.resourceURI = vjson.resourceURI;
		newCharacter.urls = vjson.urls;
		newCharacter.thumbnail = vjson.thumbnail;
		newCharacter.stories = vjson.stories;
		newCharacter.events = vjson.events;
		newCharacter.series = vjson.series;
		if(vjson.comics.returned > 0) {
			newCharacter.comics = vjson.comics.items.slice(0, 4);
		}
		return newCharacter;
	}
}
