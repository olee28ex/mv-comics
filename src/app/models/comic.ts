export default class Comic {
	id: string;
	title: string;
	desc: string;
	thumbnail: { path: string, extension: string }
	price: number = 0;

	set description(val:string) { this.desc = val; }
	get description() {
		return (this.desc && this.desc.length > 0 ) ? this.desc : 'Comic without description.';
	}
	
	getPicture() {
		let t = this.thumbnail;
		return `${t.path}.${t.extension}`;
	}

	static parse(vjson:any) {
		let newComic = new Comic();
		newComic.id = vjson.id;
		newComic.title = vjson.title;
		newComic.desc = vjson.description;
		newComic.thumbnail = vjson.thumbnail;
		if(vjson.price) {
			newComic.price = vjson.price;
		} else {
			if(vjson.prices &&vjson.prices.length > 0){
				newComic.price = vjson.prices[0].price;
			}
		}
		return newComic;
	}
}