import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BgimagenPipe } from './pipes/bgimagen.pipe';
import { TopbarComponent } from './components/topbar/topbar.component';
import { FavbarComponent } from './components/favbar/favbar.component';
import { CharacterCardComponent } from './components/character-card/character-card.component';
import { PageNotFoundComponent } from './pages/page-notfound/page-notfound.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CharacterPageComponent } from './pages/character-page/character-page.component';

@NgModule({
  declarations: [
    AppComponent,
    BgimagenPipe,
    TopbarComponent,
    FavbarComponent,
    CharacterCardComponent,
    PageNotFoundComponent,
    HomePageComponent,
    CharacterPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
