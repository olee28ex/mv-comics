import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationService } from '../../services/application.service';
import Character from '../../models/character';
import Comic from '../../models/comic';

import * as _ from 'underscore';

@Component({
  selector: 'app-character-page',
  templateUrl: './character-page.component.html',
  styleUrls: ['./character-page.component.scss']
})
export class CharacterPageComponent implements OnInit {

	character: Character = undefined;
	comics: Comic[] = [];

	cloading:boolean = false;

	constructor(
		private router: Router,
		private app: ApplicationService,
		private _acRouter: ActivatedRoute
		) {
		this._acRouter.params.subscribe( params => {
			if(params.id) {
				this.app.onLoading.emit(true);
				Promise.all([
					this.app.getCharacter(params.id),
					this.app.getCharacterComics(params.id)
				]).then(
					(results:any) =>{
						this.character = Character.parse(results[0]);
						this.comics = _.map(results[1], (_comic) => Comic.parse(_comic));
					},
					() => { 
						// this.app.pageNotFound(params.id);
					}
				).finally(()=>{
					this.app.onLoading.emit(false);
				});
			} else {
			} 
		});
	}

	ngOnInit() {
	}

	onShowComic(comic:Comic) {
		this.app.onShowComic.emit( comic );
	}

}
