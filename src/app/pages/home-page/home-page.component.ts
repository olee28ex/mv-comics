import { Component, AfterContentInit } from '@angular/core';
import { ApplicationService } from '../../services/application.service';
import { Pagination } from '../../lib/pagination';
import Character from '../../models/character';
import * as _ from 'underscore';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements AfterContentInit {
	
	_characters:Character[] = undefined;
	pagination: Pagination = new Pagination();
	strkey:string = '';
	constructor(private app: ApplicationService) {
		this.app.onSearch.subscribe((strkey:string) => {
			console.log(`Buscando => ${strkey}`);
			this.pagination.numpage = 1;
			this.strkey = strkey;
			this.updateData();
		});
	}


	ngAfterContentInit() {
		this.app.onLoading.emit(true);
		this.pagination.numpage = 3;
		this.updateData();
	}


	gotoPage(page:number) {
		console.log('page', page);
		if(page > 0 && page < (this.pagination.total_pages+1)) {
			console.log('page', page);
			this.pagination.numpage = page;
			this.updateData();
		}
	}

	updateData() {
		this.app.onLoading.emit(true);
		let params = `&limit=10`;
		if(this.strkey.length > 0){
			params += `&nameStartsWith=${this.strkey}`;
		}
		params += `&offset=${this.pagination.getOffset()}`;
		this.app.getCharacters(params)
		.then(
			(resp:any) => {
				this.app.onLoading.emit(false);
				this.pagination.total = resp.total;
				this.pagination.offset = resp.offset;
				this._characters = _.map(resp.results, (chr) => Character.parse(chr) );
				console.log(this._characters);
			},
			(err:any) => {
				this.app.onLoading.emit(false);
				console.error(err);
		});
	}

}
