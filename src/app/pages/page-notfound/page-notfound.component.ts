import { Component } from '@angular/core';
import { ApplicationService } from '../../services/application.service'
@Component({
  selector: 'app-page-notfound',
  templateUrl: './page-notfound.component.html',
  styleUrls: ['./page-notfound.component.scss']
})
export class PageNotFoundComponent {

  constructor(public app: ApplicationService) { }

}
