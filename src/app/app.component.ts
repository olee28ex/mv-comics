import { Component, AfterContentInit } from '@angular/core';
import { ApplicationService } from './services/application.service';
import Character from './models/character';
import Comic from './models/comic';
import * as _ from 'underscore';

declare var $:any;
// import html2canvas from 'html2canvas';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterContentInit {
  
  title = 'comics';
  loading:boolean = false;
  openbar:boolean = false;

  modalbg: any = {};

  comic: Comic = undefined;

  constructor(private app: ApplicationService) {
    this.app.onLoading.subscribe((_state:boolean) => {
      setTimeout(() => this.loading = _state, 600);
    });
    this.app.onShowComic.subscribe((_comic:Comic) => {
      // setTimeout(() => this.loading = _state, 600);
      this.comic = _comic;
      $('#staticBackdrop').modal('show');
    });
  }

  ngAfterContentInit() {
    // $('#staticBackdrop').modal('show');
  }


  chkFavourite(id:string) {
    return this.app.chkFavourite(id);
  }
  
  setFavourite(comic) {
    if(!this.app.chkFavourite(comic.id))
      this.app.setFavourite(comic);
    else
      this.app.delFavourite(comic.id);
  }

}
