import { Component, Input } from '@angular/core';
import Character from '../../models/character';
import Comic from '../../models/comic';
import { ApplicationService } from '../../services/application.service';
@Component({
  selector: '.character-card',
  templateUrl: './character-card.component.html',
  styleUrls: ['./character-card.component.scss']
})
export class CharacterCardComponent {

	@Input() character: Character;

	constructor(private app: ApplicationService) { }

	openComic(url) {
		console.log(url);
		this.app.get(url).toPromise()
		.then((res:any) => res.data )
		.then(
			(res)=> {
				let vjson = Comic.parse(res.results[0]);
				this.app.onShowComic.emit( vjson );
			},
			(err:any)=> {

		});
	}
}
