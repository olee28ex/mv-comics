import { Component, AfterContentInit } from '@angular/core';
import { ApplicationService } from '../../services/application.service';

@Component({
  selector: '.mv-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements AfterContentInit {

	ktimer:any= undefined;

	constructor(private app: ApplicationService) { }

  ngAfterContentInit() {
  }

  keyupSearch(event:KeyboardEvent, strkey:string) {
  	event.stopPropagation();
  	clearTimeout(this.ktimer);
  	if(event.keyCode == 13) {
  		this.app.onSearch.emit(strkey);
  	} else {
		this.ktimer = setTimeout(()=>{
			this.app.onSearch.emit(strkey);
		}, 500);
  	}
  	return false;
  }

}
