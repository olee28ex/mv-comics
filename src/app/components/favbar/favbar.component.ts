import { Component } from '@angular/core';
import { Storage } from '../../lib/storage';
import { ApplicationService } from '../../services/application.service';

import * as _ from 'underscore';

declare var $:any;

@Component({
  selector: '.favourites-bar',
  templateUrl: './favbar.component.html',
  styleUrls: ['./favbar.component.scss']
})
export class FavbarComponent {

	constructor(public app: ApplicationService) { }

	fpop(idme, id:string) {
		console.log(idme);
		$(idme).removeClass('fadeIn');
		$(idme).addClass('zoomOut');
		setTimeout(()=>{
			this.app.delFavourite(id);
		}, 600);
	}


}
