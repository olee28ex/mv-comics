import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { API_PATH, MARVEL_AUTH } from '../lib/constants';
import { Storage } from '../lib/storage';
import Comic from '../models/comic';

import * as _ from 'underscore';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

	_favourites: Comic[] = [];
	sw:boolean = false;

	onSearch: EventEmitter<string> = new EventEmitter();
	onLoading: EventEmitter<boolean> = new EventEmitter();
	onShowComic: EventEmitter<Comic> = new EventEmitter();

	set favourites(favs:Comic[]) {
		this._favourites = favs;
		Storage.setAll('mvc-favourites', this._favourites);
	}
	get favourites() { 
		if(!this.sw) {
			this.sw = true;
			let cache = Storage.getAll('mvc-favourites'); 
			if(Array.isArray(cache)) {
				this._favourites = _.map(cache, (c) =>{ return  Comic.parse(c) });
			}
			else {
				this._favourites = [];
			}
		}
		return this._favourites;
	}

	constructor(
		private router: Router,
		private _http:HttpClient,
		private location: Location
	) { }

  	get(url: string, params: string = '') {
  		return this._http.get(`${url}${MARVEL_AUTH}${params}`);
  	}

  	chkFavourite(cid:string) {
  		return _.findIndex(this._favourites, (fav:Comic) => fav.id == cid) >= 0;
  	}
  	setFavourite(_comic:Comic) {
  		if(!this.chkFavourite(_comic.id)) {
  			this._favourites.push(_comic);
  		}
  		Storage.setAll('mvc-favourites', this._favourites);
  	}
  	
  	delFavourite(cid:string) {
  		this.favourites = _.filter(this._favourites, (fav:Comic) => fav.id != cid);
  	}

  	getToken() { return MARVEL_AUTH; }

	loadComics() { return this.get('comics').toPromise(); }

	getCharacter(id:string, params:string = '') {
		return this.get(`${API_PATH}characters/${id}`, params+'')
			.toPromise()
			.then( (resp:any) => resp.data.results[0]);
	}
	getCharacterComics(id:string, params:string = '') {
		return this.get(`${API_PATH}characters/${id}/comics`, params+'')
			.toPromise()
			.then( (resp:any) => resp.data.results);
	}
	getCharacters(params:string = '') {
		return this.get(`${API_PATH}characters`, params+'&orderBy=-modified')
			.toPromise()
			.then( (resp:any) => resp.data);
	}
	getComic(url) {
		return this.get(`${url}`).toPromise().then((resp:any) => resp.data );
	}

	goHome() { this.router.navigate(['/']) }
	goBack() { this.location.back(); }
}
