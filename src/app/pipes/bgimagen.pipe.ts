import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bgimagen'
})
export class BgimagenPipe implements PipeTransform {

  transform(value: any): any {
  	let img = value ? value : '/assets/images/no-image.gif';
    return { 'background-image': `url('${img}')`};
  }

}
